# Gemlog Style Guide

## File Naming
All files should be named as the date (YYYY-MM-DD)) and title with - replacing the spaces.

### Why
This allows proper sorting while also being able to quickly see what content is in the file.

### Example
```
2020-04-22-My-First-Gemlog.gmi
```

## Title 
All gemlogs should contain a title denoted by a single `#` with one whitespace afterwards, then the title. The title doesn't have to be the first line, but it should be the first header in the document.

### Why
Besides # denoting h1, which is common for titles, the single space afterwards looks nice if the gemini client does not render the #.

### Example
```
# My First Gemlog
```

## Sections
Sections should be denoted by two `#` which corresponds to h2 with white space above, no whitespace below, and a single white space after the two `##`.

### Why
Sectioning posts keeps things nice and tidy, while using h2 keeps the content broken separated well without needing unnecessary whitespace.

### Example
```
...
Here is some content for the previous section 

## Foo Section
Here is the first sentence of the Foo Section
```

## Links
TODO

## Navigation
TODO
