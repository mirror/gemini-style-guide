# gemini-style-guide

Reading nytpu's post (1) on formatting definitely got me thinking about formatting my gemlog and gemini site. When first building it, I realized how much there is to think about (even in a simple system like gemini). I began writing a reply (2) to nytpu and thus began this repo to create a centralized style guide for all things gemini.

(1): gemini://nytpu.com/gemlog/2020-09-05.gmi

(2): gemini://gemini.circumlunar.space/~swiftmandolin/gemlog/2020-09-07-re-experimenting-with-ways-to-format-documents-on-gemini.gmi
## Table of Contents
[general best practices](general-practices.md)

[gemlog](gemlog.md)

To contribute, please see [the contribution guide](https://tildegit.org/swiftmandolin/gemini-style-guide/src/branch/master/CONTRIBUTING.md)
