# General Best Practices

## File Naming
Filenames should use '-' to replace spaces, and should have no special characters. Gemtext documents should have the extension `.gmi` or `.gemini`. Mixed case can be used, but if you are on a case sensitive filesystem (ext4, etc) you should not have 2 files with the same name and different cases (e.g. 'hello-world.gmi' and 'HELLO-WORLD.gmi')

### Why
This follows the convention for the web and ensures compatibility with most filesystems. Dashes are preferred to underscores as a separator because underscores can be hidden by an underline if a client underlines links.

### Example
```
my-cool-page.gmi
```

## Preformatted alt text
The alt text after the opening preformatted text line (`````text here``) should be used as an alt text describing the content if it is not machine-readable. For instance, ASCII art should have a brief description in the alt text, but a code block does not need alt text.

### Why
This ensures that people using gemini with something like a screen reader are able to understand all the aspects of the site.

### Example
    ```ascii banner saying "ART"
        _    ____ _____
       / \  |  _ \_   _|
      / _ \ | |_) || |
     / ___ \|  _ < | |
    /_/   \_\_| \_\|_|
    ```

## Links
TODO: probably similar to gemlog links

## Navigation
TODO: probably similar to gemlog links with some modifications

