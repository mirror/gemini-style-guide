# How to contribute to this style guide

The most important aspect for contributing to this is to discuss what should be added and even more importantly, why. Most of the work in this repo will be issues labeled `discussion`. Once one topic has been agreed upon, we can move on to changes and pull requests. So if you think something should be added, please submit a discussion issue where we can all talk about it.

To submit changes, please open a pull request (after forking) and submit your changes.

I only have a few rules for formatting which I'll explain below:

- Use `##` for each section
- Use `###` for the subsection headers
- Provide a rule
- Provide the reasoning for the rule under "Why?" subsection
- Provide an example (or multiple examples for edge cases)

